public interface Strategy {
    Operation getStrategy(int totalValue);
}
