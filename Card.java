
public class Card {
              private Rank rank;
              private Suit suit;

              public Card(Suit suit, Rank rank){
                  this.suit =suit;
                  this.rank = rank;
              }

              public String toString(){
               return "Suit ="+ suit+ " Rank =" +rank;
              }

    public Rank getRank() {
        return rank;
    }

    public Suit getSuit() {
        return suit;
    }
}
