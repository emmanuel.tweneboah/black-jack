import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class DeckTest {

    @org.junit.jupiter.api.Test
    void checkSizeOfDeck() {
        Deck deck = new Deck();
        int expectedSize =52;
        int actualSize = deck.getSize();

        assertEquals(actualSize,expectedSize);
    }
}