public class DefaultStrategy implements Strategy{
    @Override
    public Operation getStrategy(int totalValue) {
         if (totalValue >=17)
        {
            return Operation.STICK;

        }
        else if (totalValue>21)
        {
       return Operation.BUST;
        }
        return Operation.HIT;
    }
}
