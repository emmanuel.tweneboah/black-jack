import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class Deck {
    public List<Card> getDeck() {
        return deck;
    }

    List<Card> deck = new ArrayList<>();

    public Deck(){
        for(Suit  suit: Suit.values()){
            for(Rank rank : Rank.values()){
                deck.add(new Card(suit,rank));
            }
        }
    }
    public void randomize(){
      Collections.shuffle(deck);
    }
    public int getSize(){
       return  deck.size();
    }
}
