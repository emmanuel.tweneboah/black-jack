import java.util.ArrayList;
import java.util.List;

public class Player {

//    private enum strategy;
    private int total=0;
    private String name;
    List<Card> cards = new ArrayList<>();

    public Player(String name) {
        this.name = name;
    };

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public List<Card> getCards() {
        return cards;
    }

    public void setCards(Card card) {
        this.cards.add(card);
    }
}
